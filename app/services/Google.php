<?php 
namespace App\Services;

class Google
{
    protected $client;

    function __construct()
    {
		$cpath = base_path().'\credentials.json';
        $client = new \Google_Client();
        $client->setApplicationName('Google Calendar API PHP Quickstart');
		$client->setScopes(\Google_Service_Calendar::CALENDAR_READONLY);
		$client->setAuthConfig($cpath);
		$client->setApprovalPrompt('force');
		$client->setAccessType('offline');
		$client->setIncludeGrantedScopes(true);   // incremental auth
		$client->addScope(\Google_Service_Oauth2::USERINFO_EMAIL);
		$client->addScope(\Google_Service_Calendar::CALENDAR_READONLY);
		$client->setPrompt('select_account consent');
        $this->client = $client;
    }
	public function index()
    {
        $cpath = base_path().'\credentials.json';
        $client = new \Google_Client();
        $client->setApplicationName('Google Calendar API PHP Quickstart');
		$client->setScopes(\Google_Service_Calendar::CALENDAR_READONLY);
		$client->setAuthConfig($cpath);
		$client->setApprovalPrompt('force');
		$client->setAccessType('offline');
		$client->setIncludeGrantedScopes(true);   // incremental auth
		$client->addScope(\Google_Service_Oauth2::USERINFO_EMAIL);
		$client->addScope(\Google_Service_Calendar::CALENDAR_READONLY);
		$client->setPrompt('select_account consent');

        return $client;
    }
	
}
