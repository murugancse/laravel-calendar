<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Google;
use \Google_Service_Calendar;
use \Google_Service_Oauth2;
use URL;
use DB;
use App\Event;

class CalendarController extends Controller
{
    function __construct()
    {
        
    }
	public function index(Google $google){
		$client = $google->index();
		//print_r($client);die;
		$tokenPath = base_path().'\token.json';
		file_put_contents($tokenPath, '');
		if (file_exists($tokenPath)) {
			$accessToken = json_decode(file_get_contents($tokenPath), true);
			//$google->setAccessToken($accessToken);
		}

		// If there is no previous token or it's expired.
		if ($client->isAccessTokenExpired()) {
			// Refresh the token if possible, else fetch a new one.
			if ($client->getRefreshToken()) {
				$client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
			} else {
				// Request authorization from the user.
				$authUrl = $client->createAuthUrl();
				return redirect($authUrl);
			}
		}
	}
	public function viewEvent(Request $request, Google $google){
		$client = $google->index();
		$tokenPath = base_path().'\token.json';
		
		if(isset($_GET['code'])){
			if ($client->isAccessTokenExpired()) {
				if ($client->getRefreshToken()) {
					$client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
				} 
			}
			$client->authenticate($_GET['code']);
			//echo $_GET['code'];
			
			if (!file_exists(dirname($tokenPath))) {
				mkdir(dirname($tokenPath), 0700, true);
			}
			file_put_contents($tokenPath, json_encode($client->getAccessToken()));
		}
		
		$accessToken = json_decode(file_get_contents($tokenPath), true);
		if ($accessToken == null ) {
			$auth_url = $client->createAuthUrl();
			return redirect($auth_url);
		}
		
		$service = new Google_Service_Calendar($client);


		// Print the next 10 events on the user's calendar.
		$calendarId = 'primary';
		$optParams = array(
		  'maxResults' => 10,
		  'orderBy' => 'startTime',
		  'singleEvents' => true,
		  'timeMin' => date('c'),
		);
		//print_r($service->events);die;
		$results = $service->events->listEvents($calendarId);
		//print_r($results);
		$events = $results->getItems();
		
		$google_oauth = new Google_Service_Oauth2($client);
		$emailid = $google_oauth->userinfo->get()->email;

		if (empty($events)) {
			return redirect()->route('home');
		} else {
				//echo '<pre>'; print_r($events);die;
			foreach ($events as $event) {
				$results = Event::where('google_id', '=', $event->id)->get();
				if(count($results) == 0){
					$dbevent = new Event;
					$dbevent->gmail = $emailid;
					$dbevent->google_id = $event->id;
					$dbevent->summary = $event->summary;
					$dbevent->link = $event->htmlLink;
					$dbevent->save();
				}
			}
			return redirect()->route('google.list', $emailid);
		}
	}
	public function listEvent($emailid){
		$results = Event::where('gmail', '=', $emailid)->get();
		//return $results;
		return view('event-list',["events"=>$results]);
	}
}
